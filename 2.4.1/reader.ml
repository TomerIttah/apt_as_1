#use "pc.ml";;
open PC;;

type sexpr =
  | Nil
  | Symbol of string
  | Pair of sexpr * sexpr;;

(* START PARSER: general_patterns.ml *)
  let make_paired nt_left nt_right nt =
    let nt = caten nt_left nt in
    let nt = pack nt (function (_, e) -> e) in
    let nt = caten nt nt_right in
    let nt = pack nt (function (e, _) -> e) in
    nt;;

  let nt_whitespace = const (fun ch -> ch <= ' ');;
  let nt_whitespace_list = pack nt_whitespace (fun c -> [c]);;
  let make_spaced nt = make_paired (star nt_whitespace) (star nt_whitespace) nt;;
  let make_left_spaced nt = make_paired (star nt_whitespace) nt_epsilon nt;;
  let nt_skip nt sexp = make_paired (star nt_whitespace_list) (star nt_whitespace_list) nt;;
  let make_nt_round_brackets_expr nt = make_paired (make_spaced (char '(')) (make_spaced (char ')')) nt;;
(* END PARSER: general_patterns.ml *)

(* START PARSER: symbol_parser.ml *)
  let letters = range_ci 'a' 'z';;
  let nt_symbol nt = nt_skip (pack (make_spaced (plus letters)) (function h -> Symbol (list_to_string (List.map lowercase_ascii h)))) nt;;
(* END PARSER: symbol_parser.ml *)

(* START PARSER: list_parser.ml *)
let nt_dotted_list nt = nt_skip (pack (make_nt_round_brackets_expr (caten (caten (nt_skip (plus nt) nt) (make_spaced (char '.'))) (plus nt)))
                       (fun ((h, dot), t) -> match ((h, dot), t) with
                         | ((h :: arg :: args, '.'), t :: []) -> Pair (h, Pair(arg,
                           List.fold_right (fun e acc -> (Pair (Symbol "lambda", (Pair (e, acc))))) args t))
                         | ((h :: arg :: args, '.'), t :: z) -> Pair (h, Pair(arg,
                           List.fold_right (fun e acc -> (Pair (Symbol "lambda", (Pair (e, acc))))) args
                           (List.fold_left (fun acc e -> (Pair (acc, Pair(e, Nil)))) t z)))
                         | ((_,_),_) -> raise X_no_match)) nt;;


  let nt_list nt = nt_skip (pack (make_nt_round_brackets_expr (nt_skip (star nt) nt))
                    (fun s -> match (s) with
                    | (t :: z) -> (List.fold_left (fun acc e -> (Pair (acc, Pair(e, Nil)))) t z)
                    | _ -> List.fold_right (fun e acc -> (Pair (e, acc))) s Nil)) nt;;
(* END PARSER: list_parser.ml *)

(* START PARSER: sexpr_parser.ml *)
  let nt_sexpr =
    let rec nt () = disj_list [(nt_symbol (delayed nt)); (nt_list (delayed nt)); (nt_dotted_list (delayed nt))] in nt ();;
(* END PARSER: sexpr_parser.ml *)

module Reader: sig
  val read_sexpr : string -> sexpr
end
= struct
let normalize_scheme_symbol str =
  let s = string_to_list str in
  if (andmap
	(fun ch -> (ch = (lowercase_ascii ch)))
	s) then str
  else Printf.sprintf "|%s|" str;;

let read_sexpr string = (fun (h, t) -> match (h, t) with
                          | (h, []) -> h
                          | (_,_) -> raise X_no_match) (nt_sexpr (string_to_list string));;

end;;
