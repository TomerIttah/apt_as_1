#use "topfind";;
#require "oUnit";;

#use "tag-parser.ml";;

open Tag_Parser;;
open OUnit2;;

let test_lambda_exp_0 text_ctxt = assert_equal (Abstraction ("x", Var "x")) (Tag_Parser.tag_parse_expression (Pair (Symbol "lambda", Pair (Symbol "x", Symbol "x"))));;
let test_lambda_exp_1 text_ctxt = assert_equal (Abstraction ("p", Abstraction ("a", Abstraction ("b", Var "a")))) (Tag_Parser.tag_parse_expression (Pair (Symbol "lambda",
                                                                                                                       Pair (Symbol "p",
                                                                                                                        Pair (Symbol "lambda",
                                                                                                                         Pair (Symbol "a",
                                                                                                                          Pair (Pair (Symbol "lambda", Pair (Symbol "b", Symbol "a")), Nil)))))));;
(* (lambda p (p a)) *)
let test_lambda_exp_2 text_ctxt = assert_equal (Abstraction ("p", Applic (Var "p", Var "a"))) (Tag_Parser.tag_parse_expression (Pair (Symbol "lambda",
                                                                                                                       Pair (Symbol "p", Pair (Symbol "p", Pair (Symbol "a", Nil))))));;
(* ((lambda p . (p (lambda a . (lambda b . a)))) (lambda x . (a b))) *)
let test_lambda_exp_3 text_ctxt = assert_equal (Applic
                                                 (Abstraction ("p",
                                                   Applic (Var "p", Abstraction ("a", Abstraction ("b", Var "a")))),
                                                 Abstraction ("x", Applic (Var "x", Applic (Var "a", Var "b"))))) (Tag_Parser.tag_parse_expression (Pair
                                                                                                                       (Pair (Symbol "lambda",
                                                                                                                         Pair (Symbol "p",
                                                                                                                          Pair (Symbol "p",
                                                                                                                           Pair
                                                                                                                            (Pair (Symbol "lambda",
                                                                                                                              Pair (Symbol "a",
                                                                                                                               Pair (Pair (Symbol "lambda", Pair (Symbol "b", Symbol "a")), Nil))),
                                                                                                                            Nil)))),
                                                                                                                       Pair
                                                                                                                        (Pair (Symbol "lambda",
                                                                                                                          Pair (Symbol "x",
                                                                                                                           Pair (Symbol "x", Pair (Symbol "a", Pair (Symbol "b", Nil))))),
                                                                                                                        Nil))
                                                                                                                      ));;

(* let test_lambda_exp_4 text_ctxt = assert_equal () (Tag_Parser.tag_parse_expression ()) *)

let tag_parser_tester_suite =
"tag_parser_tester_suite">:::
 ["test_lambda_exp_0">:: test_lambda_exp_0;
 "test_lambda_exp_1">:: test_lambda_exp_1;
 "test_lambda_exp_2">:: test_lambda_exp_2;
 "test_lambda_exp_3">:: test_lambda_exp_3;
 (* "test_lambda_exp_4">:: test_lambda_exp_4; *)
  ]
;;

let () =
  run_test_tt_main tag_parser_tester_suite
;;
