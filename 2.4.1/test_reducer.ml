#use "topfind";;
#require "oUnit";;

#use "reducer.ml";;

open Reducer;;
open OUnit2;;

let test_lambda_exp_0 text_ctxt = assert_equal ("I") (Reducer.reduce_to_bases "(lambda x . x)");;
let test_lambda_exp_1 text_ctxt = assert_equal ("S") (Reducer.reduce_to_bases "(lambda x . (lambda y . (lambda z . ((x z) (y z)))))");;
let test_lambda_exp_2 text_ctxt = assert_equal ("K") (Reducer.reduce_to_bases "(lambda x . (lambda y . x))");;
let test_lambda_exp_3 text_ctxt = assert_equal ("B") (Reducer.reduce_to_bases "(lambda x . (lambda y . (lambda z . (x (y z)))))");;
let test_lambda_exp_4 text_ctxt = assert_equal ("C") (Reducer.reduce_to_bases "(lambda x . (lambda y . (lambda z . ((x z) y))))");;

let test_lambda_exp_0_la text_ctxt = assert_equal ("I") (Reducer.reduce_to_bases "(lambda x . x)");;
let test_lambda_exp_1_la text_ctxt = assert_equal ("S") (Reducer.reduce_to_bases "(lambda x y z . x z (y z))");;
let test_lambda_exp_2_la text_ctxt = assert_equal ("K") (Reducer.reduce_to_bases "(lambda x y . x)");;
let test_lambda_exp_3_la text_ctxt = assert_equal ("B") (Reducer.reduce_to_bases "(lambda x y z . x (y z))");;
let test_lambda_exp_4_la text_ctxt = assert_equal ("C") (Reducer.reduce_to_bases "(lambda x y z . x z y)");;

let test_lambda_exp_5 text_ctxt = assert_equal ("((S ((B C) ((B (B B)) (B B)))) C)") (Reducer.reduce_to_bases "(lambda x . (lambda y . (lambda z . (lambda t . ((x y) ((x t) z))))))");;
let test_lambda_exp_6 text_ctxt = assert_equal ("((C ((B B) S)) (C I))") (Reducer.reduce_to_bases "(lambda x . (lambda y . (lambda z . ((x z) (z y)))))");;
let test_lambda_exp_7 text_ctxt = assert_equal ("((S I) I)") (Reducer.reduce_to_bases "(lambda a . (lambda b . ((a a) b)))");;
let test_lambda_exp_8 text_ctxt = assert_equal ("(K (K ((S I) I)))") (Reducer.reduce_to_bases "(lambda a . (lambda b . (lambda a . (a a))))");;
let test_lambda_exp_9 text_ctxt = assert_equal ("(K K)") (Reducer.reduce_to_bases "(lambda a . (lambda b . (lambda a . b)))");;

let test_lambda_exp_5_la text_ctxt = assert_equal ("((S ((B C) ((B (B B)) (B B)))) C)") (Reducer.reduce_to_bases "(lambda x y z t . x y (x t z))");;
let test_lambda_exp_6_la text_ctxt = assert_equal ("((C ((B B) S)) (C I))") (Reducer.reduce_to_bases "(lambda x y z . x z (z y))");;
let test_lambda_exp_7_la text_ctxt = assert_equal ("((S I) I)") (Reducer.reduce_to_bases "(lambda a b . a a b)");;
let test_lambda_exp_8_la text_ctxt = assert_equal ("(K (K ((S I) I)))") (Reducer.reduce_to_bases "(lambda a b a . a a)");;
let test_lambda_exp_9_la text_ctxt = assert_equal ("(K K)") (Reducer.reduce_to_bases "(lambda a b a . b)");;

let test_c0 text_ctxt = assert_equal ("(K I)") (Reducer.reduce_to_bases "(lambda s . (lambda z . z))");;
let test_splus text_ctxt = assert_equal ("(S B)") (Reducer.reduce_to_bases "(lambda n . (lambda s . (lambda z . (s ((n s) z)))))");;
let test_cplus text_ctxt = assert_equal ("((C I) (S B))") (Reducer.reduce_to_bases "(lambda ca . (lambda cb . ((ca (lambda n . (lambda s . (lambda z . (s ((n s) z)))))) cb)))");;
let test_cmult text_ctxt = assert_equal ("((C ((B C) ((B (C I)) ((C I) (S B))))) (K I))") (Reducer.reduce_to_bases "(lambda ca . (lambda cb . ((cb ((lambda ca . (lambda cb . ((ca (lambda n . (lambda s . (lambda z . (s ((n s) z)))))) cb))) ca)) (lambda s . (lambda z . z)))))");;
let test_pi_12 text_ctxt = assert_equal ("((C I) K)") (Reducer.reduce_to_bases "(lambda p . (p (lambda a . (lambda b . a))))");;
let test_pi_22 text_ctxt = assert_equal ("((C I) (K I))") (Reducer.reduce_to_bases "(lambda p . (p (lambda a . (lambda b . b))))");;
let test_factorial text_ctxt = assert_equal ("((B ((C I) (K I))) ((C ((C I) ((S ((B C) ((B (C I)) ((B (S B)) ((C I) K))))) ((S ((B ((C ((B C) ((B (C I)) ((C I) (S B))))) (K I))) ((C I) K))) ((C I) (K I)))))) ((C ((C I) ((S B) (K I)))) ((S B) (K I)))))") (Reducer.reduce_to_bases "(lambda n . ((lambda p . (p (lambda a . (lambda b . b)))) ((n (lambda p . (lambda x . ((x ((lambda n . (lambda s . (lambda z . (s ((n s) z))))) ((lambda p . (p (lambda a . (lambda b . a)))) p))) (((lambda ca . (lambda cb . ((cb ((lambda ca . (lambda cb . ((ca (lambda n . (lambda s . (lambda z . (s ((n s) z)))))) cb))) ca)) (lambda s . (lambda z . z))))) ((lambda p . (p (lambda a . (lambda b . a)))) p)) ((lambda p . (p (lambda a . (lambda b . b)))) p)))))) (lambda x . ((x ((lambda n . (lambda s . (lambda z . (s ((n s) z))))) (lambda s . (lambda z . z)))) ((lambda n . (lambda s . (lambda z . (s ((n s) z))))) (lambda s . (lambda z . z))))))))");;

let test_c0_la text_ctxt = assert_equal ("(K I)") (Reducer.reduce_to_bases "(lambda s z . z)");;
let test_splus_la text_ctxt = assert_equal ("(S B)") (Reducer.reduce_to_bases "(lambda n s z . s (n s z))");;
let test_cplus_la text_ctxt = assert_equal ("((C I) (S B))") (Reducer.reduce_to_bases "(lambda ca cb . ca (lambda n s z . s (n s z)) cb)");;
let test_cmult_la text_ctxt = assert_equal ("((C ((B C) ((B (C I)) ((C I) (S B))))) (K I))") (Reducer.reduce_to_bases "(lambda ca cb . cb ((lambda ca cb . ca (lambda n s z . s (n s z)) cb) ca) (lambda s z . z))");;
let test_pi_12_la text_ctxt = assert_equal ("((C I) K)") (Reducer.reduce_to_bases "(lambda p . p (lambda a b . a))");;
let test_pi_22_la text_ctxt = assert_equal ("((C I) (K I))") (Reducer.reduce_to_bases "(lambda p . p (lambda a b . b))");;

(* Assosiativity Tests *)
let test_left_assosiativity_0 text_ctxt = assert_equal (Reducer.reduce_to_bases "(lambda s . (lambda z . ((s (s s)) z)))")
  (Reducer.reduce_to_bases "(lambda s z . s (s s) z)");;
let test_left_assosiativity_1 text_ctxt = assert_equal (Reducer.reduce_to_bases "(lambda s . (lambda z . (lambda n . (((n n) (s s)) (z z)))))")
  (Reducer.reduce_to_bases "(lambda s z n . n n (s s) (z z))");;
let test_left_assosiativity_2 text_ctxt = assert_equal (Reducer.reduce_to_bases "(lambda s . (lambda z . (lambda n . ((s (s s)) (z (n n))) )))")
  (Reducer.reduce_to_bases "(lambda s z n . s (s s) (z (n n)))");;
let test_left_assosiativity_3 text_ctxt = assert_equal (Reducer.reduce_to_bases "(lambda a . (lambda b . ((((a a) a) b) (a b)) ))")
  (Reducer.reduce_to_bases "(lambda a b . a a a b (a b))");;

let reducer_tester_suite =
"reducer_tester_suite">:::
 ["test_lambda_exp_0">:: test_lambda_exp_0;
 "test_lambda_exp_1">:: test_lambda_exp_1;
 "test_lambda_exp_2">:: test_lambda_exp_2;
 "test_lambda_exp_3">:: test_lambda_exp_3;
 "test_lambda_exp_4">:: test_lambda_exp_4;

 "test_lambda_exp_0_la">:: test_lambda_exp_0_la;
 "test_lambda_exp_1_la">:: test_lambda_exp_1_la;
 "test_lambda_exp_2_la">:: test_lambda_exp_2_la;
 "test_lambda_exp_3_la">:: test_lambda_exp_3_la;
 "test_lambda_exp_4_la">:: test_lambda_exp_4_la;

 "test_lambda_exp_5">:: test_lambda_exp_5;
 "test_lambda_exp_6">:: test_lambda_exp_6;
 "test_lambda_exp_7">:: test_lambda_exp_7;
 "test_lambda_exp_8">:: test_lambda_exp_8;
 "test_lambda_exp_9">:: test_lambda_exp_9;

 "test_lambda_exp_5_la">:: test_lambda_exp_5_la;
 "test_lambda_exp_6_la">:: test_lambda_exp_6_la;
 "test_lambda_exp_7_la">:: test_lambda_exp_7_la;
 "test_lambda_exp_8_la">:: test_lambda_exp_8_la;
 "test_lambda_exp_9_la">:: test_lambda_exp_9_la;

 "test_c0">:: test_c0;
 "test_splus">:: test_splus;
 "test_cplus">:: test_cplus;
 "test_cmult">:: test_cmult;
 "test_pi_12">:: test_pi_12;
 "test_pi_22">:: test_pi_22;
 "test_factorial">:: test_factorial;

 "test_c0_la">:: test_c0_la;
 "test_splus_la">:: test_splus_la;
 "test_cplus_la">:: test_cplus_la;
 "test_cmult_la">:: test_cmult_la;
 "test_pi_12_la">:: test_pi_12_la;
 "test_pi_22_la">:: test_pi_22_la;

 "test_left_assosiativity_0">:: test_left_assosiativity_0;
 "test_left_assosiativity_1">:: test_left_assosiativity_1;
 "test_left_assosiativity_2">:: test_left_assosiativity_2;
 "test_left_assosiativity_3">:: test_left_assosiativity_3;
  ]
;;

let () =
  run_test_tt_main reducer_tester_suite
;;
