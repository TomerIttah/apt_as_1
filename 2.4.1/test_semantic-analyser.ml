#use "topfind";;
#require "oUnit";;

#use "semantic-analyser.ml";;

open Semantics;;
open OUnit2;;

let test_lambda_exp_0 text_ctxt = assert_equal (Abstraction' ("x", Var' (VarParam ("x", 0)))) (Semantics.run_semantics (Abstraction ("x", Var "x")));;
let test_lambda_exp_1 text_ctxt = assert_equal (Applic'
                                                 (Abstraction' ("p",
                                                   Applic' (Var' (VarParam ("p", 0)),
                                                    Abstraction' ("a", Abstraction' ("b", Var' (VarBound ("a", 0, 0)))))),
                                                 Abstraction' ("x",
                                                  Applic' (Var' (VarParam ("x", 0)),
                                                   Applic' (Var' (VarFree "a"), Var' (VarFree "b")))))) (Semantics.run_semantics (Applic
                                                                                                         (Abstraction ("p",
                                                                                                           Applic (Var "p", Abstraction ("a", Abstraction ("b", Var "a")))),
                                                                                                         Abstraction ("x", Applic (Var "x", Applic (Var "a", Var "b"))))));;

let semantic_analyser_tester_suite =
"semantic_analyser_tester_suite">:::
 ["test_lambda_exp_0">:: test_lambda_exp_0;
 "test_lambda_exp_1">:: test_lambda_exp_1;
  ]
;;

let () =
  run_test_tt_main semantic_analyser_tester_suite
;;
