#use "tag-parser.ml";;

type var =
  | VarFree of string
  | VarParam of string * int
  | VarBound of string * int * int;;

type base =
  | I
  | K
  | B
  | C
  | S

type expr' =
  | Base' of base
  | Var' of var
  | Abstraction' of string * expr'
  | Applic' of expr' * expr';;

  (* Gets an element and a list, if the element is a member of the list then
   return the first occurence index, otherwise return -1 *)
  let get_member_index e l =
    let rec get_member_index_rec = (fun e l i -> match l with
      | [] -> -1
      | h::t -> if (h = e) then i else (get_member_index_rec e t (i + 1))) in
                get_member_index_rec e l 0;;

  (* Gets an element and a list of lists, if the element is a member of one of
    the lists then return a tuple contians <major, minor> that matched the first
    occurence, otherwise reutrn <-1, -1> *)
  let get_major_minor_indexes e ll =
    let rec get_major_minor_indexes_rec = (fun e ll major minor -> match ll with
      | [] -> (-1, -1)
      | h::t -> let i = (get_member_index e h) in
                    if (i != -1) then (major, i) else
                    (get_major_minor_indexes_rec e t (major + 1) 0)) in
                    get_major_minor_indexes_rec e ll 0 0;;


exception X_syntax_error;;

module type SEMANTICS = sig
  val run_semantics : expr -> expr'
  val annotate_lexical_addresses : expr -> expr'
  val reannotate_lexical_addresses : expr' -> expr'
end;;

module Semantics : SEMANTICS = struct

let annotate_lexical_addresses e =
  let rec generate_lexical_addresses = (fun e args -> match e with
    | Var(v) -> let (major, minor) = (get_major_minor_indexes v args) in
                (match (major, minor) with
                  | (-1, -1) -> Var'(VarFree(v))
                  | (0, minor) -> Var'(VarParam(v, minor))
                  | (major, minor) -> Var'(VarBound(v, (major - 1), minor)))
    | Abstraction(arg_list, expr) -> Abstraction'(arg_list,
                                                   (generate_lexical_addresses expr ([arg_list] :: args)))
    | Applic(expr, expr_list) -> Applic'((generate_lexical_addresses expr args),
                                         (generate_lexical_addresses expr_list args))) in
    generate_lexical_addresses e [];;

let reannotate_lexical_addresses e =
  let rec regenerate_lexical_addresses = (fun e args -> match e with
    | Var'(VarFree(v)) -> let (major, minor) = (get_major_minor_indexes v args) in
                (match (major, minor) with
                  | (-1, -1) -> Var'(VarFree(v))
                  | (0, minor) -> Var'(VarParam(v, minor))
                  | (major, minor) -> Var'(VarBound(v, (major - 1), minor)))
    | Var'(VarBound(v, _, _)) -> let (major, minor) = (get_major_minor_indexes v args) in
                (match (major, minor) with
                  | (-1, -1) -> Var'(VarFree(v))
                  | (0, minor) -> Var'(VarParam(v, minor))
                  | (major, minor) -> Var'(VarBound(v, (major - 1), minor)))
    | Var'(VarParam(v, _)) -> let (major, minor) = (get_major_minor_indexes v args) in
                (match (major, minor) with
                  | (-1, -1) -> Var'(VarFree(v))
                  | (0, minor) -> Var'(VarParam(v, minor))
                  | (major, minor) -> Var'(VarBound(v, (major - 1), minor)))
    | Abstraction'(arg_list, expr) -> Abstraction'(arg_list,
                                                   (regenerate_lexical_addresses expr ([arg_list] :: args)))
    | Applic'(expr, expr_list) -> Applic'((regenerate_lexical_addresses expr args),
                                         (regenerate_lexical_addresses expr_list args))
    | t -> t) in regenerate_lexical_addresses e [];;


let run_semantics expr =
       annotate_lexical_addresses expr;;

end;;
