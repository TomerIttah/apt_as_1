#use "semantic-analyser.ml";;

exception X_not_combinator;;

(* Bases:
  Assumptions:
    1 - Currying.
    2 - Left assosiative brackets.

  I = (lambda x . x) Identitatsfunktion
  K = (lambda x y . x) Konstanzfunktion
  B = (lambda x y z . x (yz)) Zusammensetzungsfunktion
  C = (lambda x y z . x z y) Vertauschungsfunktion
  S = (lambda x y z . x z (y z)) Verschmelzungsfunktion
 *)

module type REDUCER = sig
  val run_reducer : expr' -> string
  val reduce_to_bases : string -> string
end;;

module Reducer : REDUCER = struct

let find_var var e1 =
  let rec find_var_in_expr = (fun var e1 nested -> match e1 with
    | Var'(VarParam(v, _)) -> if v = var && nested = 0 then true else false
    | Var'(VarBound(v, major, _)) -> if v = var && major = (nested - 1) then true else false
    | Abstraction'(arg, expr) -> if arg = var then false else find_var_in_expr var expr (nested + 1)
    | Applic'(expr1, expr2) -> find_var_in_expr var expr1 nested || find_var_in_expr var expr2 nested
    | _ -> false) in find_var_in_expr var e1 0;;

let annotate_bases e =
  let rec annotate_to_bases = (fun e -> match e with
    | Abstraction'(arg1, Abstraction'(arg2, expr2)) -> (annotate_to_bases (Semantics.reannotate_lexical_addresses (Abstraction'(arg1, (annotate_to_bases (Abstraction'(arg2, expr2)))))))
    | Applic'(expr1, expr2) -> Applic'((annotate_to_bases expr1), (annotate_to_bases expr2))
    | Abstraction'(arg1,  Applic'(expr1, Var'(VarParam(arg2, _)))) when arg1 = arg2 && (not (find_var arg1 expr1)) -> (annotate_to_bases expr1)
    | Abstraction'(arg, Var'(VarParam(_, 0))) -> Base'(I)
    | Abstraction'(arg, expr) when (not (find_var arg expr)) -> Applic'(Base'(K), (annotate_to_bases expr))
    | Abstraction'(arg,  Applic'(expr1, expr2)) -> let (a, b) = (find_var arg expr1, find_var arg expr2) in (match (a, b) with
      | true, true -> Applic'(Applic'(Base'(S), (annotate_to_bases (Semantics.reannotate_lexical_addresses (Abstraction'(arg, expr1))))), (annotate_to_bases (Semantics.reannotate_lexical_addresses (Abstraction'(arg, expr2)))))
      | true, false -> Applic'(Applic'(Base'(C), (annotate_to_bases (Semantics.reannotate_lexical_addresses (Abstraction'(arg, expr1))))), (annotate_to_bases expr2))
      | false, true -> Applic'(Applic'(Base'(B), (annotate_to_bases expr1)), (annotate_to_bases (Semantics.reannotate_lexical_addresses (Abstraction'(arg, expr2)))))
      | _ -> Applic'(Base'(K), Applic'(expr1, expr2)))
    | t -> t) in annotate_to_bases e;;

let reduce e =
  let rec reduce_expr = (fun e -> match e with
    | Base'(I) -> "I"
    | Base'(K) -> "K"
    | Base'(B) -> "B"
    | Base'(C) -> "C"
    | Base'(S) -> "S"
    | Applic'(expr1, expr2) -> "(" ^ (reduce_expr expr1) ^ " " ^ (reduce_expr expr2) ^ ")"
    | _ -> raise X_not_combinator) in reduce_expr e;;

let run_reducer expr =
  reduce
    (annotate_bases expr);;

let reduce_to_bases expr =
  (run_reducer (Semantics.run_semantics (Tag_Parser.tag_parse_expression (Reader.read_sexpr expr))));;

end;;
