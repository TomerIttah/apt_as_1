#use "reader.ml";;

open Reader;;

type expr =
  | Var of string
  | Abstraction of string * expr
  | Applic of expr * expr;;

exception X_syntax_error;;

module type TAG_PARSER = sig
  val tag_parse_expression : sexpr -> expr
  val tag_parse_expressions : sexpr list -> expr list
end;;

module Tag_Parser : TAG_PARSER = struct

let reserved_word_list = ["lambda"];;

let tag_parse_expression sexpr =
  let rec tag_parse = fun (s) -> match s with
    | Pair(Symbol("lambda"), Pair(Symbol(x), Pair(exp, Nil))) -> Abstraction(x, (tag_parse exp))
    | Pair(Symbol("lambda"), Pair(Symbol(x), sexp)) -> Abstraction(x, (tag_parse sexp))
    | Symbol (s) -> if (not(List.exists (fun p -> p = s) reserved_word_list)) then Var(s) else raise X_syntax_error
    | Pair(sexp, Pair (sexp_list, Nil)) -> Applic(tag_parse sexp, tag_parse sexp_list)
    | Pair(sexp, sexp_list) -> Applic(tag_parse sexp, tag_parse sexp_list)
    | _ -> raise X_syntax_error in tag_parse sexpr;;;;

let tag_parse_expressions sexpr = (List.map (fun s -> (tag_parse_expression s)) sexpr);;

end;;
