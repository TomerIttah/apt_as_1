#lang lazy
(define c0
  (lambda (s)
    (lambda (z)
      z)))

(define s+
  (lambda (n)
    (lambda (s)
      (lambda (z)
	(s ((n s) z))))))

(define c+
  (lambda (ca)
    (lambda (cb)
      ((ca s+) cb))))

(define c*
  (lambda (ca)
    (lambda (cb)
      ((cb (c+ ca)) c0))))

(define integer->church
  (lambda (n)
    (if (zero? n)
	c0
	(s+ (integer->church (- n 1))))))

;;; \s z.(s (s ... (s z) ... ))
(define church->integer
  (lambda (cn)
    ((cn (lambda (n) (+ n 1)))
     0)))

(define pi_12 (lambda (p) (p (lambda (a) (lambda (b) a)))))
(define pi_22 (lambda (p) (p (lambda (a) (lambda (b) b)))))
(define f (lambda (p) (lambda (x) ((x (s+ (pi_12 p))) ((c* (pi_12 p)) (pi_22 p))))))
(define factorial (lambda (n) (pi_22 ((n f) (lambda (x) ((x (s+ c0)) (s+ c0)))))))
(define fpredecessor (lambda (p) (lambda (x) ((x (pi_22 p)) (s+ (pi_22 p))))))
(define predecessor (lambda (n) (pi_12 ((n fpredecessor) (lambda (x) ((x c0) c0))))))
(define church_monus (lambda (a) (lambda (b) ((b predecessor) a))))
(define church_true (lambda (x) (lambda (y) x)))
(define church_false (lambda (x) (lambda (y) y)))
(define church_zero? (lambda (n) ((n (lambda (x) church_false)) church_true)))
(define church_and (lambda (a) (lambda (b) ((a b) church_false))))
(define church_ge (lambda (a) (lambda (b) (church_zero? ((church_monus b) a)))))
(define church_le (lambda (a) (lambda (b) (church_zero? ((church_monus a) b)))))
(define church_= (lambda (a) (lambda (b) ((church_and ((church_ge a) b)) ((church_le a) b)))))

; Question: 2.3.1
(define Ichurch (lambda (cn) (((church_zero? cn) c0) (s+ (Ichurch (predecessor cn))))))
(define Fchurch (lambda (Ichurch) (lambda (cn) (((church_zero? cn) c0) (s+ (Ichurch (predecessor cn)))))))

(define Ycurry
  (lambda (f)
    ((lambda (x)
       (f (lambda args
            (apply (x x) args))))
     (lambda (x)
       (f (lambda args
            (apply (x x) args)))))))

(define P (Ycurry Fchurch))

(define Fchurch-nothing (lambda (cn) (error 'Fchurch-nothing "can't compute (Fchurch ~a)" (church->integer cn))))
(define Fchurch0 (Fchurch Fchurch-nothing))
(define Fchurch1 (Fchurch Fchurch0))
(define Fchurch2 (Fchurch Fchurch1))
(define Fchurch3 (Fchurch Fchurch2))

; church = limit n->infinity Fchurchn = lim n->infinity (Fchurch^n emptyset)
; will raise an error -(church->integer (Fchurch2 (s+ (s+ (s+ c0)))))
; Tests (normal order evaluation is required!)
(= (church->integer (Fchurch3 c0)) 0)
(= (church->integer (Fchurch3 (s+ c0))) 1)
(= (church->integer (Fchurch3 (s+ (s+ c0)))) 2)
(= (church->integer (Fchurch3 (s+ (s+ (s+ c0))))) 3)
(= (church->integer (P (integer->church 0))) 0)
(= (church->integer (P (integer->church 1))) 1)
(= (church->integer (P (integer->church 13))) 13)
(= (church->integer (P (integer->church 22))) 22)
