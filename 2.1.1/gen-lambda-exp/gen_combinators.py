import argparse

from functools import reduce
from typing import Dict, List


class Expr:
    def __init__(self, nested: int, combinations: int, length: int, var_arr: List[int]):
        self.nested: int = nested
        self.combinations: int = combinations
        self.length: int = length
        self.var_arr: List[int] = var_arr

    def __len__(self):
        return self.length


class Var(Expr):
    def __init__(self, name: str):
        super().__init__(0, 0, 1, [0])
        self.name: str = name

    def __repr__(self):
        return self.name


class Abstraction(Expr):
    def __init__(self, arg: str, exp: Expr):
        super().__init__(exp.nested + 1,
                         reduce(lambda acc, e: acc * (e + 1), exp.var_arr, 1),
                         1 + len(exp),
                         [(x + 1) for x in exp.var_arr])
        self.arg: str = arg
        self.exp: Expr = exp

    def __repr__(self):
        return "(lambda " + str(self.arg) + " . " + str(self.exp) + ")"


class Applic(Expr):
    def __init__(self, expr1: Expr, expr2: Expr):
        super().__init__(min(expr1.nested, expr2.nested),
                         0 if expr1.nested == 0 or expr2.nested == 0 else expr1.combinations * expr2.combinations,
                         1 + len(expr1) + len(expr2),
                         expr1.var_arr + expr2.var_arr)
        self.expr1: Expr = expr1
        self.expr2: Expr = expr2

    def __repr__(self):
        return "(" + str(self.expr1) + " " + str(self.expr2) + ")"


def get_all_combinators_by_length(length: int) -> List[Expr]:
    # level i = (lambda v . exp) s.t. exp in level i - 1 UNION
    # (exp1 exp2) s.t. exp1 in level j exp2 in level i - j - 2 for all 0 <= j <= i - 2
    # WHERE combinator in level i is of length i + 1
    lambda_expressions: Dict[int, List[Expr]] = {0: [Var('v')]}

    for i in range(1, length):
        lambda_expressions[i] = []
        for exp in lambda_expressions[i - 1]:
            lambda_expressions[i].append(Abstraction('v', exp))

        for j in range(i - 1):
            for prev_exp in lambda_expressions[j]:
                for exp in lambda_expressions[i - j - 2]:
                    lambda_expressions[i].append(Applic(exp, prev_exp))

    return list(filter(lambda e: e.combinations != 0, lambda_expressions[length - 1]))


def main(length: int):
    count: int = 0
    for expr in get_all_combinators_by_length(length):
        print("Combinator: %s\nAvailable combinations: %d" % (expr, expr.combinations))
        count += expr.combinations

    print("Total combinators of length %d: %d" % (length, count))


def parse_program_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("-l", "--length", help="returns all combinators of length l", type=int, default=2)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_program_arguments()
    main(args.length)
