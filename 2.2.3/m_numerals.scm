;; Church Numerals
(define c0
  (lambda (s)
    (lambda (z)
      z)))

(define s+
  (lambda (n)
    (lambda (s)
      (lambda (z)
	(s ((n s) z))))))

(define integer->church
  (lambda (n)
    (if (zero? n)
	c0
	(s+ (integer->church (- n 1))))))

(define church->integer
  (lambda (cn)
    ((cn (lambda (n) (+ n 1)))
     0)))

;; M Numerals
(define m0 (lambda (p) p))

(define m_s+
  (lambda (mn)
    (lambda (s)
      (s (mn s)))))

(define m+
  (lambda (ma)
    (lambda (mb)
    ((ma (lambda (x)
          ((x (lambda (m)
                 (lambda (b)
                   (lambda (a)
                     (lambda (x)
                       ((x m) (m_s+ b))))))) mb)))
    (lambda (m)
      (lambda (n)
       n))))))

(define m*
  (lambda (ma)
    (lambda (mb)
      ((ma (lambda (x)
             ((x (lambda (m)
                   (lambda (b)
                     (lambda (a)
                       (lambda (x)
                         ((x m) ((m+ b) mb))))))) m0)))
    (lambda (m)
      (lambda (n)
       n))))))


(define m_true (lambda (x) (lambda (y) x)))

(define m_false (lambda (x) (lambda (y) y)))

(define m_zero?
  (lambda (mn)
    ((mn (lambda (x)
          ((x (lambda (m)
                 (lambda (b)
                   (lambda (a)
                     (lambda (x)
                       ((x m) m_false)))))) m_true)))
    (lambda (m)
      (lambda (n)
       n)))))

(define m_numerals->integer
  (lambda (mn)
    ((mn (lambda (x)
          ((x (lambda (m)
                 (lambda (b)
                   (lambda (a)
                     (lambda (x)
                       ((x m) (+ 1 b))))))) 0)))
    (lambda (m)
      (lambda (n)
       n)))))

(define integer->m_numerals
  (lambda (n)
    ((zero? n)
     m0
     (m_s+ (integer->m_numerals (- n 1))))
    )
  )

(define m_numerals->church
  (lambda (mn)
    ((mn (lambda (x)
          ((x (lambda (m)
                 (lambda (b)
                   (lambda (a)
                     (lambda (x)
                       ((x m) (s+ b))))))) c0)))
    (lambda (m)
      (lambda (n)
       n)))))

(define church->m_numerals
  (lambda (cn)
    ((cn m_s+) m0)
    )
  )

; t_zero? Tests
(and
 (= (m_numerals->integer (((m_zero? m0) m0) (m_s+ m0))) 0)
 (= (m_numerals->integer (((m_zero? m0) (m_s+ m0)) (m_s+ m0))) 1)
 (= (m_numerals->integer (((m_zero? m0) (m_s+ (m_s+ m0))) (m_s+ m0))) 2)
 (= (m_numerals->integer (((m_zero? (m_s+ m0)) m0) m0)) 0)
 (= (m_numerals->integer (((m_zero? (m_s+ m0)) m0) (m_s+ m0))) 1)
 (= (m_numerals->integer (((m_zero? (m_s+ m0)) m0) (m_s+ (m_s+ m0)))) 2)
 (= (m_numerals->integer (((m_zero? (m_s+ (m_s+ m0))) (m_s+ (m_s+ m0))) (m_s+ m0))) 1)
 (= (m_numerals->integer (((m_zero? (m_s+ (m_s+ (m_s+ m0)))) (m_s+ (m_s+ m0))) (m_s+ (m_s+ (m_s+ m0))))) 3)
 )

; Successor Tests
(and
 (= (m_numerals->integer m0) 0)
 (= (m_numerals->integer (m_s+ m0)) 1)
 (= (m_numerals->integer (m_s+ (m_s+ m0))) 2)
 (= (m_numerals->integer (m_s+ (m_s+ (m_s+ m0)))) 3)
 (= (m_numerals->integer (m_s+ (m_s+ (m_s+ (m_s+ m0))))) 4)
 (= (m_numerals->integer (m_s+ (m_s+ (m_s+ (m_s+ (m_s+ m0)))))) 5)
 (= (m_numerals->integer (m_s+ (m_s+ (m_s+ (m_s+ (m_s+ (m_s+ m0))))))) 6)
 )

; Addition Tests
(and
 (= (m_numerals->integer ((m+ m0) m0)) 0)
 (= (m_numerals->integer ((m+ (m_s+ m0)) m0)) 1)
 (= (m_numerals->integer ((m+ m0) (m_s+ m0))) 1)
 (= (m_numerals->integer ((m+ (m_s+ m0)) (m_s+ m0))) 2)
 (= (m_numerals->integer ((m+ (m_s+ (m_s+ (m_s+ (m_s+ m0))))) (m_s+ (m_s+ m0)))) 6)
 (= (m_numerals->integer ((m+ (m_s+ (m_s+ m0)) )(m_s+ (m_s+ (m_s+ (m_s+ m0)))))) 6)
 (= (m_numerals->integer ((m+ (m_s+ (m_s+ m0))) m0)) 2)
 (= (m_numerals->integer ((m+ m0) (m_s+ (m_s+ m0)))) 2)
 )

; Multiplication Tests
(and
 (= (m_numerals->integer ((m* m0) m0)) 0)
 (= (m_numerals->integer ((m* (m_s+ m0)) m0)) 0)
 (= (m_numerals->integer ((m* m0) (m_s+ m0))) 0)
 (= (m_numerals->integer ((m* m0) (m_s+ (m_s+ m0)))) 0)
 (= (m_numerals->integer ((m* (m_s+ (m_s+ m0))) m0)) 0)
 (= (m_numerals->integer ((m* (m_s+ m0)) (m_s+ m0))) 1)
 (= (m_numerals->integer ((m* (m_s+ m0)) (m_s+ (m_s+ m0)))) 2)
 (= (m_numerals->integer ((m* (m_s+ (m_s+ m0))) (m_s+ m0))) 2)
 (= (m_numerals->integer ((m* (m_s+ (m_s+ m0))) (m_s+ (m_s+ m0)))) 4)
 (= (m_numerals->integer ((m* (m_s+ (m_s+ (m_s+ m0)))) (m_s+ (m_s+ m0)))) 6)
 (= (m_numerals->integer ((m* (m_s+ (m_s+ m0))) (m_s+ (m_s+ (m_s+ m0))))) 6)
 (= (m_numerals->integer ((m* (m_s+ (m_s+ (m_s+ m0)))) (m_s+ (m_s+ (m_s+ m0))))) 9)
 (= (m_numerals->integer ((m* (m_s+ (m_s+ (m_s+ m0)))) (m_s+ (m_s+ (m_s+ (m_s+ m0)))))) 12)
 (= (m_numerals->integer ((m* (m_s+ (m_s+ (m_s+ (m_s+ m0))))) (m_s+ (m_s+ (m_s+ m0))))) 12)
 )

; m_numerals->church church->m_numerals Tests
(and
 (= (m_numerals->integer (church->m_numerals (m_numerals->church m0))) 0)
 (= (m_numerals->integer (church->m_numerals (m_numerals->church (m_s+ m0)))) 1)
 (= (m_numerals->integer (church->m_numerals (m_numerals->church (m_s+ (m_s+ m0))))) 2)
 (= (m_numerals->integer (church->m_numerals (m_numerals->church (m_s+ (m_s+ (m_s+ m0)))))) 3)
 (= (m_numerals->integer (church->m_numerals (m_numerals->church (m_s+ (m_s+ (m_s+ (m_s+ m0))))))) 4)
 (= (m_numerals->integer (church->m_numerals (m_numerals->church (m_s+ (m_s+ (m_s+ (m_s+ (m_s+ m0)))))))) 5)
 (= (m_numerals->integer (church->m_numerals (m_numerals->church (m_s+ (m_s+ (m_s+ (m_s+ (m_s+ (m_s+ m0))))))))) 6)
 )

 ; m_numerals->church Tests
 (and
  (= (church->integer (m_numerals->church m0)) 0)
  (= (church->integer (m_numerals->church (m_s+ m0))) 1)
  (= (church->integer (m_numerals->church (m_s+ (m_s+ m0)))) 2)
  (= (church->integer (m_numerals->church (m_s+ (m_s+ (m_s+ m0))))) 3)
  (= (church->integer (m_numerals->church (m_s+ (m_s+ (m_s+ (m_s+ m0)))))) 4)
  (= (church->integer (m_numerals->church (m_s+ (m_s+ (m_s+ (m_s+ (m_s+ m0))))))) 5)
  (= (church->integer (m_numerals->church (m_s+ (m_s+ (m_s+ (m_s+ (m_s+ (m_s+ m0)))))))) 6)
  )
