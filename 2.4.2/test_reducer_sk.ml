#use "topfind";;
#require "oUnit";;

#use "reducer_sk.ml";;

open ReducerSK;;
open OUnit2;;

let test_lambda_exp_I text_ctxt = assert_equal ("((S K) K)") (ReducerSK.reduce_to_bases "(lambda x . x)");;
let test_lambda_exp_S text_ctxt = assert_equal ("S") (ReducerSK.reduce_to_bases "(lambda x . (lambda y . (lambda z . ((x z) (y z)))))");;
let test_lambda_exp_K text_ctxt = assert_equal ("K") (ReducerSK.reduce_to_bases "(lambda x . (lambda y . x))");;
let test_lambda_exp_B text_ctxt = assert_equal ("((S (K S)) K)") (ReducerSK.reduce_to_bases "(lambda x . (lambda y . (lambda z . (x (y z)))))");;
let test_lambda_exp_C text_ctxt = assert_equal ("((S ((S (K S)) ((S (K K)) S))) (K K))") (ReducerSK.reduce_to_bases "(lambda x . (lambda y . (lambda z . ((x z) y))))");;
let test_lambda_exp_5 text_ctxt = assert_equal ("(S ((S (K S)) K))") (ReducerSK.reduce_to_bases "(lambda a . (lambda b . (lambda c . (b ((a b) c)))))");;
let test_lambda_exp_6 text_ctxt = assert_equal ("((S ((S K) K)) ((S K) K))") (ReducerSK.reduce_to_bases "(lambda a . (lambda b . ((a a) b)))");;

let test_c0 text_ctxt = assert_equal ("(K ((S K) K))") (ReducerSK.reduce_to_bases "(lambda s . (lambda z . z))");;
let test_splus text_ctxt = assert_equal ("(S ((S (K S)) K))") (ReducerSK.reduce_to_bases "(lambda n . (lambda s . (lambda z . (s ((n s) z)))))");;
let test_cplus text_ctxt = assert_equal ("((S ((S K) K)) (K (S ((S (K S)) K))))") (ReducerSK.reduce_to_bases "(lambda ca . (lambda cb . ((ca (lambda n . (lambda s . (lambda z . (s ((n s) z)))))) cb)))");;
let test_cmult text_ctxt = assert_equal ("((S ((S (K S)) ((S (K (S ((S K) K)))) ((S (K K)) ((S ((S K) K)) (K (S ((S (K S)) K)))))))) (K (K (K ((S K) K)))))") (ReducerSK.reduce_to_bases "(lambda ca . (lambda cb . ((cb ((lambda ca . (lambda cb . ((ca (lambda n . (lambda s . (lambda z . (s ((n s) z)))))) cb))) ca)) (lambda s . (lambda z . z)))))");;
let test_pi_12 text_ctxt = assert_equal ("((S ((S K) K)) (K K))") (ReducerSK.reduce_to_bases "(lambda p . (p (lambda a . (lambda b . a))))");;
let test_pi_22 text_ctxt = assert_equal ("((S ((S K) K)) (K (K ((S K) K))))") (ReducerSK.reduce_to_bases "(lambda p . (p (lambda a . (lambda b . b))))");;

let test_lambda_exp_0_la text_ctxt = assert_equal ("S") (ReducerSK.reduce_to_bases "(lambda x y z . x z (y z))");;
let test_lambda_exp_1_la text_ctxt = assert_equal ("K") (ReducerSK.reduce_to_bases "(lambda x y . x)");;
let test_lambda_exp_2_la text_ctxt = assert_equal ("(K K)") (ReducerSK.reduce_to_bases "(lambda a b a . b)");;

let test_left_assosiativity_0 text_ctxt = assert_equal (ReducerSK.reduce_to_bases "(lambda s . (lambda z . ((s (s s)) z)))")
  (ReducerSK.reduce_to_bases "(lambda s z . s (s s) z)");;
let test_left_assosiativity_1 text_ctxt = assert_equal (ReducerSK.reduce_to_bases "(lambda s . (lambda z . (lambda n . (((n n) (s s)) (z z)))))")
  (ReducerSK.reduce_to_bases "(lambda s z n . n n (s s) (z z))");;
let test_left_assosiativity_2 text_ctxt = assert_equal (ReducerSK.reduce_to_bases "(lambda s . (lambda z . (lambda n . ((s (s s)) (z (n n))) )))")
  (ReducerSK.reduce_to_bases "(lambda s z n . s (s s) (z (n n)))");;
let test_left_assosiativity_3 text_ctxt = assert_equal (ReducerSK.reduce_to_bases "(lambda a . (lambda b . ((((a a) a) b) (a b)) ))")
  (ReducerSK.reduce_to_bases "(lambda a b . a a a b (a b))");;


let reducer_sk_tester_suite =
"reducer_sk_tester_suite">:::
 ["test_lambda_exp_I">:: test_lambda_exp_I;
 "test_lambda_exp_S">:: test_lambda_exp_S;
 "test_lambda_exp_K">:: test_lambda_exp_K;
 "test_lambda_exp_B">:: test_lambda_exp_B;
 "test_lambda_exp_C">:: test_lambda_exp_C;
 "test_lambda_exp_5">:: test_lambda_exp_5;
 "test_lambda_exp_6">:: test_lambda_exp_6;

 "test_c0">:: test_c0;
 "test_splus">:: test_splus;
 "test_cplus">:: test_cplus;
 "test_cmult">:: test_cmult;
 "test_pi_12">:: test_pi_12;
 "test_pi_22">:: test_pi_22;

 "test_lambda_exp_0_la">:: test_lambda_exp_0_la;
 "test_lambda_exp_1_la">:: test_lambda_exp_1_la;
 "test_lambda_exp_2_la">:: test_lambda_exp_2_la;

 "test_left_assosiativity_0">:: test_left_assosiativity_0;
 "test_left_assosiativity_1">:: test_left_assosiativity_1;
 "test_left_assosiativity_2">:: test_left_assosiativity_2;
 "test_left_assosiativity_3">:: test_left_assosiativity_3;
  ]
;;

let () =
  run_test_tt_main reducer_sk_tester_suite
;;
