#use "topfind";;
#require "oUnit";;

#use "reader.ml";;

open Reader;;
open OUnit2;;

let test_lambda_exp_0 text_ctxt = assert_equal (Pair (Symbol "lambda", Pair (Symbol "x", Symbol "x"))) (Reader.read_sexpr "(lambda x . x)");;

(* Name the test cases and group them together *)
let reader_tester_suite =
"reader_tester_suite">:::
 ["test_lambda_exp_0">:: test_lambda_exp_0;
  ]
;;

let () =
  run_test_tt_main reader_tester_suite
;;
