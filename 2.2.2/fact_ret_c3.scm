(define c0
  (lambda (s)
    (lambda (z)
      z)))

(define s+
  (lambda (n)
    (lambda (s)
      (lambda (z)
	(s ((n s) z))))))

(define c+
  (lambda (ca)
    (lambda (cb)
      ((ca s+) cb))))

(define c*
  (lambda (ca)
    (lambda (cb)
      ((cb (c+ ca)) c0))))

(define integer->church
  (lambda (n)
    (if (zero? n)
	c0
	(s+ (integer->church (- n 1))))))

;;; \s z.(s (s ... (s z) ... ))
(define church->integer
  (lambda (cn)
    ((cn (lambda (n) (+ n 1)))
     0)))

(define pi_12 (lambda (p) (p (lambda (a) (lambda (b) a)))))
(define pi_22 (lambda (p) (p (lambda (a) (lambda (b) b)))))
(define f (lambda (p) (lambda (x) ((x (s+ (pi_12 p))) ((c* (pi_12 p)) (pi_22 p))))))
(define factorial (lambda (n) (pi_22 ((n f) (lambda (x) ((x (s+ c0)) (s+ c0)))))))

; Question: 2.2.2
(define lambda_trick (lambda (x) (lambda (y) (lambda (z) (s+ (s+ (s+ c0)))))))
(church->integer (factorial lambda_trick))
