(define c0
  (lambda (s)
    (lambda (z)
      z)))

(define s+
  (lambda (n)
    (lambda (s)
      (lambda (z)
	(s ((n s) z))))))

(define c+
  (lambda (ca)
    (lambda (cb)
      ((ca s+) cb))))

(define c*
  (lambda (ca)
    (lambda (cb)
      ((cb (c+ ca)) c0))))

(define integer->church
  (lambda (n)
    (if (zero? n)
	c0
	(s+ (integer->church (- n 1))))))

;;; \s z.(s (s ... (s z) ... ))
(define church->integer
  (lambda (cn)
    ((cn (lambda (n) (+ n 1)))
     0)))

(define pi_12 (lambda (p) (p (lambda (a) (lambda (b) a)))))
(define pi_22 (lambda (p) (p (lambda (a) (lambda (b) b)))))
(define f (lambda (p) (lambda (x) ((x (s+ (pi_12 p))) ((c* (pi_12 p)) (pi_22 p))))))
(define factorial (lambda (n) (pi_22 ((n f) (lambda (x) ((x (s+ c0)) (s+ c0)))))))
(define fpredecessor (lambda (p) (lambda (x) ((x (pi_22 p)) (s+ (pi_22 p))))))
(define predecessor (lambda (n) (pi_12 ((n fpredecessor) (lambda (x) ((x c0) c0))))))
(define Ycurry
  (lambda (f)
    ((lambda (x)
       (f (lambda args
            (apply (x x) args))))
     (lambda (x)
       (f (lambda args
            (apply (x x) args)))))))

; Question: 2.3.3
(define s0 (lambda (x) (lambda (y) x)))
(define s1 (lambda (x) (lambda (y) (y (lambda (x) (lambda (y) x))))))
(define s2 (lambda (x) (lambda (y) (y s1))))
(define s3 (lambda (x) (lambda (y) (y s2))))
(define s4 (lambda (x) (lambda (y) (y s3))))
(define s5 (lambda (x) (lambda (y) (y s4))))
; scott succesor
(define s+scott (lambda (n) (lambda (x) (lambda (y) (y n)))))
; scott addition
(define +scott (Ycurry (lambda (f) (lambda (a) (lambda (b) ((a b) (lambda (n) (s+scott ((f n) b)))))))))
; Scott multiplication
(define *scott
  (Ycurry
   (lambda (f)
     (lambda (a)
       (lambda (b)
         ((a s0)
          (lambda (n)
            ((+scott b) ((f n) b)))))))))

(define integer->scott
  (lambda (n)
    (if (zero? n)
	s0
	(s+scott (integer->scott (- n 1))))))

;(sn+1 <ReturnIfZero> <ApplyIfNonZero>) = (<ApplyIfNonZero> sn)
(define scott->integer
  (lambda (sn)
    ((sn 0) (lambda (n) (+ 1 (scott->integer n)))))
    )

(define scott->integer->fixed
  (Ycurry
   (lambda (f)
     (lambda (sn)
       ((sn 0) (lambda (n) (+ 1 (f n))))))
    )
  )

(define scott->church
  (lambda (sn)
    ((sn c0)(lambda (cn) ((c+ (s+ c0)) (scott->church cn)))))
    )

(define scott->church->fixed
  (Ycurry
   (lambda (f)
     (lambda (sn)
       ((sn c0)(lambda (cn) ((c+ (s+ c0)) (f cn)))))
     )
   )
  )

(define church->scott
  (lambda (cn)
    ((cn (lambda (sn) ((+scott sn) s1)))
     s0)))

; tests scott->church church->scott
(and
 (= (church->integer (scott->church->fixed s0)) 0)
 (= (church->integer (scott->church->fixed s1)) 1)
 (= (church->integer (scott->church->fixed s2)) 2)
 (= (church->integer (scott->church->fixed s3)) 3)
 (= (church->integer (scott->church->fixed s4)) 4)
 (= (scott->integer->fixed (church->scott c0)) 0)
 (= (scott->integer->fixed (church->scott (s+ c0))) 1)
 (= (scott->integer->fixed (church->scott (s+ (s+ c0)))) 2)
 (= (scott->integer->fixed (church->scott (s+ (s+ (s+ c0))))) 3)
 (= (scott->integer->fixed (church->scott (s+ (s+ (s+ (s+ c0)))))) 4)
 )

; tests scott multiplication
(and
 (= (scott->integer->fixed ((*scott s0) s1)) 0)
 (= (scott->integer->fixed ((*scott s1) s0)) 0)
 (= (scott->integer->fixed ((*scott s1) s1)) 1)
 (= (scott->integer->fixed ((*scott s1) s2)) 2)
 (= (scott->integer->fixed ((*scott s2) s2)) 4)
 (= (scott->integer->fixed ((*scott s4) s3)) 12)
 (= (scott->integer->fixed ((*scott s0) s3)) 0)
 (= (scott->integer->fixed ((*scott s4) s0)) 0)
 (= (scott->integer->fixed ((*scott s2) s3)) 6)
 (= (scott->integer->fixed ((*scott s4) s4)) 16)
 )

; Factscott
(define Factscott
  (Ycurry
   (lambda (Ffactscott)
     (lambda (sn)
       ((sn s1) (lambda (n) ((*scott sn) (Ffactscott n))))))))

; tests Factscott
(and
 (= (scott->integer->fixed (Factscott s0)) 1)
 (= (scott->integer->fixed (Factscott s1)) 1)
 (= (scott->integer->fixed (Factscott s2)) 2)
 (= (scott->integer->fixed (Factscott s3)) 6)
 (= (scott->integer->fixed (Factscott s4)) 24)
 (= (scott->integer->fixed (Factscott s5)) 120)
 )
