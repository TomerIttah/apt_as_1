(define c0
  (lambda (s)
    (lambda (z)
      z)))

(define s+
  (lambda (n)
    (lambda (s)
      (lambda (z)
	(s ((n s) z))))))

(define c+
  (lambda (ca)
    (lambda (cb)
      ((ca s+) cb))))

(define c*
  (lambda (ca)
    (lambda (cb)
      ((cb (c+ ca)) c0))))

(define integer->church
  (lambda (n)
    (if (zero? n)
	c0
	(s+ (integer->church (- n 1))))))

;;; \s z.(s (s ... (s z) ... ))
(define church->integer
  (lambda (cn)
    ((cn (lambda (n) (+ n 1)))
     0)))

(define pi_12 (lambda (p) (p (lambda (a) (lambda (b) a)))))
(define pi_22 (lambda (p) (p (lambda (a) (lambda (b) b)))))
(define f (lambda (p) (lambda (x) ((x (s+ (pi_12 p))) ((c* (pi_12 p)) (pi_22 p))))))
(define factorial (lambda (n) (pi_22 ((n f) (lambda (x) ((x (s+ c0)) (s+ c0)))))))
(define fpredecessor (lambda (p) (lambda (x) ((x (pi_22 p)) (s+ (pi_22 p))))))
(define predecessor (lambda (n) (pi_12 ((n fpredecessor) (lambda (x) ((x c0) c0))))))

; Question: 2.2.4
; Church numerals addition definition - Take 1
(define c+new_1
  (lambda (ca)
    (lambda (cb)
      ((cb s+) ca))))

; Church numerals addition definition - Take 2
(define c+new_2
  (lambda (ca)
    (lambda (cb)
      (pi_22 ((ca fpredecessor) (lambda (x) ((x ca) cb)))))))

; Tests
(and
 (=
 (church->integer ((c+ (integer->church 1)) (integer->church 1)))
 (church->integer ((c+new_1 (integer->church 1)) (integer->church 1)))
 (church->integer ((c+new_2 (integer->church 1)) (integer->church 1))))
 (=
 (church->integer ((c+ (integer->church 0)) (integer->church 0)))
 (church->integer ((c+new_1 (integer->church 0)) (integer->church 0)))
 (church->integer ((c+new_2 (integer->church 0)) (integer->church 0))))
 (=
 (church->integer ((c+ (integer->church 0)) (integer->church 9)))
 (church->integer ((c+new_1 (integer->church 0)) (integer->church 9)))
 (church->integer ((c+new_2 (integer->church 0)) (integer->church 9))))
 (=
 (church->integer ((c+ (integer->church 21)) (integer->church 13)))
 (church->integer ((c+new_1 (integer->church 21)) (integer->church 13)))
 (church->integer ((c+new_2 (integer->church 21)) (integer->church 13))))
 )
